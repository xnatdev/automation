/*
 * automation: org.nrg.automation.runners.RubyScriptRunner
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.automation.runners;

import org.nrg.automation.annotations.Supports;

@Supports("ruby")
public class RubyScriptRunner extends AbstractScriptRunner {
}
